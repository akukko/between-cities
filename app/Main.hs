module Main where

import Server


import Network.Wai
import Network.Wai.Handler.Warp
import Network.HTTP.Types (status200)
import Network.HTTP.Types.URI (QueryItem(..))
import Blaze.ByteString.Builder (copyByteString)
import qualified Data.ByteString.UTF8 as BU
import Data.Monoid
import Data.Binary.Builder

import Debug.Trace


main = do
    let port = 3030
    putStrLn $ "Listening on port " ++ show port
    run port app

