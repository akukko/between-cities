{-# LANGUAGE OverloadedStrings #-}

module Pages
    ( resultInputPage
    , infoPage
    , scoreboardPage
    ) where

import Result

import Text.Blaze.Html5 as H hiding (map)

import Text.Blaze.Html5.Attributes as A
import qualified Text.Blaze.Html4.FrameSet.Attributes as A4
import Text.Blaze.Html.Renderer.Utf8
import Control.Monad
import qualified Data.Text as T
import Data.List
import Data.Ord
import Data.Maybe
import qualified Data.Map as M
import Text.Blaze.Internal ( textValue  )
import Network.URI

import Debug.Trace

titleText = "Kaupunnienväliset 2022"

infoTitleText = titleText <> " – Kilpailukutsu & Lajikohtaiset ohjeet"

scoreboardText = "Tulokset"
newResultText = "Uusi tulos"
infoText = "Info ja säännöt"
mainText = "Etusivu"

scoreboardUrl = "scoreboard"
newResultUrl = "new-result"
infoUrl = "info"
mainUrl = "index"

sbButton = (scoreboardText, scoreboardUrl)
newResultButton = (newResultText, newResultUrl)
infoButton = (infoText, infoUrl)
mainButton = (mainText, mainUrl)

channelLink = "https://www.youtube.com/channel/UCZl2oFJr1Tr32jMTmctMalA"
sweepRulesLink = "https://gitlab.com/akukko/kyykka-saannot/-/blob/master/smuulin-sviippitreeni.md"
sweepVideoLink = "https://youtu.be/wsdOTF_C_h4"
hydraRulesLink = "https://gitlab.com/akukko/kyykka-saannot/-/blob/master/pystyhydra.md"
hydraVideoLink = "https://youtu.be/iQzil12AzTk"

navigation buttons = do
    H.div ! A.style "display: flex;" $ do
        forM_ buttons (\(text, a) -> do
            H.form ! action a $ do
                input ! type_ "submit" ! value text)


h1Style = "background-color: powderblue"
dividerStyle = "border-top: 1px solid lightgrey"

infoPage = renderHtml $ do
    meta ! (charset "UTF-8")
    H.head $ do
        H.title titleText

    body $ do

        h1 ( infoTitleText ) ! A.style h1Style

        navigation [mainButton]
        navigation [newResultButton]

        h2 ("TL;DR;")

        h3 ("Milloin")
        H.div ( "27.8.2022 10:00 – 9.9.2022 23:59" )

        h3 ("Lajit")
        H.ul $ do
            H.li ( "Henkilökohtainen peli (HK)" )
            H.li ( "Joukkuekenttä yksin (HJK)" )
            H.li ( "5-ottelu (5o)" )
            H.li ( "7-ottelu (7o)" )
            H.li ( "Smuulin sviippitreeni (SmS)" )
            H.li ( "Bonuksena: läpäisty Pystyhydra ansaitsee kaupungille \
                   \pienen vakiomäärän pisteitä)" )

        h3 ("Tulokset")
        p ( "Ilmoitetaan tuloskilkkeeseen 'Uusi tulos'-napin kautta." )
        H.label ( "Videot suorituksista saa ")
        H.a ( "'Kaupunkienväliset'" ) ! A.href channelLink
        H.label ("–YouTube-kanavalle ottamalla yhteyttä Telegramissa @ahvena.")

        hr ! A.style dividerStyle

        h2 ( "Kilpailukutsu" )
        p ( "Kaupunkienväliset 2022 starttaa Kuopion/Suonenjoen Santun \
            \Seiska Grand Prix -kilpailun kanssa (lauantaiaamuna 27.8. kello \
            \10:00). Näin ollen kisareissu Suonenjoen priimakuntoiselle \
            \pelikentälle on mainio tilaisuus haistella Liiton kisojen \
            \megalomaanisen muikeaa tunnelmaa ja paiskoa samalla törkeän \
            \kovia KV-tuloksia Kundien ja Peltotalojen kanssa!" )
        p ( "Kilpailun tulosten ilmoittamisen takaraja on Cup-finaalia \
            \edeltävänä perjantaina 9.9.2022 kello 23:59." )
        p ( "Kaupunkienvälisten formaatin vuoksi lajikatalogia on hieman \
            \sovitettu." )

        hr ! A.style dividerStyle

        h2 ( "Kisalajit" )
        H.ul $ do
            H.li ( "Henkilökohtainen peli (HK)" )
            H.li ( "Joukkuekenttä yksin (HJK)" )
            H.li ( "5-ottelu (5o)" )
            H.li ( "7-ottelu (7o)" )
            H.li ( "Smuulin sviippitreeni (SmS)" )

        p ( "Lisäksi Pystyhydra tekee paluun, mutta tänä vuonna twistinä on, \
            \että Hydrassa ei kilpailla lajin sisällä, vaan pelin \
            \läpäisemällä ansaitsee kaupungilleen korvamerkityn vakiomäärän \
            \pisteitä." )
        p ( "Videotodiste pelistä olisi suotavaa, mutta kyykkäyhteisössä on \
            \nyt ja jatkossakin pystyttävä luottamaan harrastajien \
            \rehellisyyteen. Jos siis teet tuloksen pelikaverin todistamana, \
            \niin se voitakoon ilmoittaa mukaan kisaan." )

        hr ! A.style dividerStyle

        h2 ( "Lajikohtaiset ohjeet ja huomiot pisteenlaskusta" )

        h3 ( "Joukkuekenttä yksin (HJK):" )
        p ( "Kyykkäliiton pisteenlaskutapa. 40 heittoa käytettävissä erää \
            \kohti, eli (40 - käytetyt heitot 1. erässä) + (40 - käytetyt \
            \heitot 2. erässä) = kokonaistulos. Jos akkoja ja pappeja jää \
            \40 heiton jälkeen, niin erätulos on miinusmerkkinen ja lasketaan \
            \normaaliin tapaan (akka -2p ja pappi - 1p)." )

        h3 ( "Smuulin sviippitreeni (SmS):" )
        p ( "Pelataan kaksi erää. Maksimipisteet 80 (2 erää * 5 lajia * 8 \
            \poistettua kyykkää)" )
        H.ul $ do
            H.li $ do
                H.a ( "Säännöt" ) ! A.href sweepRulesLink
            H.li $ do
                H.a ( "Peliohje" ) ! A.href sweepVideoLink

        h3 ( "Pystyhydra (PH):" )
        p ( "Läpäisysuoritukseksi riittää puolikas hydra (1 erä). Voi pelata \
            \esteettömänä (jos pelaa yksin niin ei tarvi kasata konaa \
            \eteensä)." )
        H.ul $ do
            H.li $ do
                H.a ( "Säännöt" ) ! A.href hydraRulesLink
            H.li $ do
                H.a ( "Peliohje" ) ! A.href hydraVideoLink


resultInputPage = renderHtml $ do
    meta ! (charset "UTF-8")
    H.head $ do
        H.title titleText

    body $ do

        h1 (titleText <> " tuloskilke") ! A.style h1Style

        navigation [ infoButton ]
        navigation [ sbButton ]

        br
        h2 $ H.span "Syötä uusi tulos"
        H.div ! class_ "send-new" $ do
            H.form ! action "new-result" $ do
                H.label ! for "name" $ "Nimi: "
                input ! type_ "text" ! name "name" ! required "name"
                br
                H.label ! for "sport" $ "Laji: "
                H.select ! name "sport" ! required "sport" $ do
                    H.option ! A.style "display:none" $ ""
                    H.option "Henkkari"
                    H.option "5-ottelu"
                    H.option "7-ottelu"
                    H.option "Joukkis"
                    H.option "Sviippitreeni"
                    H.option "Pystyhydra"
                br
                H.label ! for "gender" $ "Sarja: "
                H.select ! name "gender" $ do
                    H.option "Miesten"
                    H.option "Naisten"
                br
                H.label ! for "score" $ "Tulos: "
                input ! type_ "number" ! A.step "0.5" ! A.min "-160" ! name "score" ! required "score"
                br

                H.label ! for "city" $ "Kaupunki: "
                H.select ! name "city" ! required "city" $ do
                    H.option ! A.style "display:none" $ ""
                    H.option "Oulu"
                    H.option "Tampere"
                    H.option "Lappeenranta"
                    H.option "Helsinki"
                    H.option "Rauma"
                    H.option "Muu"
                br
                H.label ! for "proof" $ "Url videoon tai kisan nimi (ei pakollinen): "
                input ! type_ "text" ! name "proof"
                br
                br

                input ! type_ "submit" ! name "Lähetä tulos" ! value "Lisää uusi tulos"



filterResults results selectedSport = do
    let sorting =
            case selectedSport of
                Pystyhydra -> sortOn score
                _         -> sortOn (Down . score)
    let filtered = filter (\(Result {sport = s}) -> s == selectedSport) results
    sorting $ findBestResults filtered M.empty

-- | This can only be used when the results are of
--   the same type (sport) i.e. only henkkari's etc.
findBestResults [] m = M.elems m
findBestResults (r@Result { player = p, score = s }:rs) m =
    findBestResults rs (M.insertWith addBetter p r m)

addBetter (new@(Result { score = sNew }))
          (old@(Result { score = sOld })) = do
    if sNew > sOld
        then new
        else old

isMen Result {series = Miehet} = True
isMen _ = False

scoreboardPage results filter = renderHtml $ do
    let filtered = maybe results ((filterResults results)) filter

    let (mens, womens) = partition isMen filtered

    let pts = pointsPerCity results

    meta ! (charset "UTF-8")

    H.head $ do
        H.title titleText

    body $ do

        h1 (titleText <> " tulokset") ! A.style h1Style

        navigation [infoButton]
        navigation [newResultButton]

        H.table ! A.style "font-size: 1em;"
            ! (A4.border "border:2px solid black;")
            ! (A.style "width:100%") $ do
                H.tr $ do forM_ ["Kaupunki", "Pisteet"] H.th
                let mapper (a, b) = mapM_ (H.td . toHtml) [ show a, show b ]
                forM_ (map mapper pts) $ H.tr

        br

        H.form ! action "scoreboard" $ do
            forM_ [ "Kaikki", "Henkkari", "Viisiottelu"
                  , "Seiskaottelu", "Joukkis"
                  , "Sviippitreeni", "Pystyhydra"
                  ] $
                (\x -> input ! type_ "submit" ! value x ! name "sport")
        forM_ [("Naiset", womens), ("Miehet", mens)] (uncurry scores)
scores title results = do
    H.table ! A.style "float: left; font-size: 1em;"
        ! (A4.border "border:2px solid black;")
        ! (A.style "width:100%") $ do
            H.tr $ do
                forM_ ["Pelaaja", "Laji", "Kaup." , "Tulos", "Todiste"] H.th
            forM_ (map (rowFromResult) results) H.tr


rowFromResult (Result plr sp se sc c p) = do
    forM_ [T.unpack plr, shortSport sp, shortCity c, show sc] $ (H.td . toHtml)
    -- Urls are made links


    let shortenedP = do
            let maxlen = 15
            if T.length p > maxlen - 1
                then T.unpack $ (T.strip (T.take maxlen p)) <> "..."
                else T.unpack p

    let def = H.td (((H.a . toHtml) (shortenedP)))
    case parseURI $ T.unpack p of
        Just url -> do
            case uriAuthority url of
                Just uriAuth ->
                    if isInfixOf "youtu" (uriRegName uriAuth)
                        then (H.td $ (H.a "Video" ! A.href (textValue p)))
                        else (H.td $ (H.a "Linkki" ! A.href (textValue p)))
                Nothing -> def
        Nothing -> def

pointsPerCity :: [Result] -> [(City, Float)]
pointsPerCity results = do
    let normalSports = [ Henkkari
                       , Viisiottelu
                       , Seiskaottelu
                       , Joukkis
                       , Sviippitreeni
                       ]
    let maxPoints = 30
    let (mens, womens) = partition isMen results

    let pts x =
            foldl (M.unionWith (+)) M.empty $
                map ( pointsFromSport M.empty maxPoints
                    . filterResults x
                    ) normalSports

    let bothPts = M.unionWith (+) (pts mens) (pts womens)


    let hydraPts x = pointsFromHydra M.empty $ filterResults x Pystyhydra
    let bothHydraPts = M.unionWith (+) (hydraPts mens) (hydraPts womens)

    sortOn (Down . snd) $ M.toList $ M.unionWith (+) bothHydraPts bothPts


pointsFromHydra m [] = m
pointsFromHydra m (Result { city = c }:rs) = do
    let hydraPts = 10
    let m' = M.insertWith (+) c hydraPts m
    pointsFromHydra m' rs

pointsFromSport m _ [] = m
pointsFromSport m pts (r@Result { city = c, score = s }:rs) = do

    let (sameScores, rest) = partition (\(Result { score = s'}) -> s == s') (r:rs)

    let amt = fromIntegral $ length sameScores
    let pts' = (((pts * amt - amt + 1)) / amt)
    let m' = splitPoints m pts' sameScores
    pointsFromSport m' (pts - 1) rest

splitPoints m pts [] = m
splitPoints m pts (Result { city = c }:rs) =
    splitPoints (M.insertWith (+) c pts m) pts rs
