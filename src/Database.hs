{-# LANGUAGE OverloadedStrings #-}


module Database ( updateDB
                , getDB
                ) where


import Result


import qualified Data.Text.IO as TextIO ( writeFile, readFile )
import Data.Text
import System.Directory
import Control.Concurrent.Lock

dbDir :: FilePath
dbDir = "db/"

dbPath :: FilePath
dbPath = dbDir <> "database"





updateDB :: Lock -> Result -> IO ()
updateDB lock newResult = do
    with lock $ do
        putStrLn ("Lisätään tulosta" ++ show newResult)
        exists <- doesFileExist dbPath
        oldDB <- if exists
                    then TextIO.readFile dbPath
                    else do
                        createDirectoryIfMissing True dbDir
                        writeFile dbPath ""
                        return ""
        TextIO.writeFile dbPath (pack (show newResult) <> "\n" <> oldDB)

getDB :: IO (Maybe Text)
getDB = do
    exists <- doesFileExist dbPath
    if exists
        then Just <$> TextIO.readFile dbPath
        else return Nothing
