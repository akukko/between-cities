{-# LANGUAGE OverloadedStrings #-}

module Server (app) where

import Pages
import Database
import Result

import Network.Wai
import Network.Wai.Handler.Warp
import Network.HTTP.Types (status200)
import Network.HTTP.Types.URI (QueryItem(..))
import Blaze.ByteString.Builder (copyByteString)
import qualified Data.ByteString.UTF8 as BU
import Data.Monoid
import Data.Binary.Builder
import qualified Data.Text as T
import qualified Data.Text.Lazy as LT
import Data.Text.Lazy.Encoding as TE

import Control.Monad.IO.Class
import Control.Concurrent.Lock

import Debug.Trace


{-
app :: MonadIO m => p -> (Response -> m b) -> m b
app req respond = do
    liftIO $ putStrLn "Said hi"
    respond $ responseLBS status200 [("Content-Type", "text/plain")] "Hi"
-}

--app :: MonadIO m => p -> (Response -> m b) -> m b
--app :: Monad m => Request -> (Response -> m t) -> m t
app req respond = do
    let path = pathInfo req
    let query = queryString req
    let msg = "Got request (" ++ show path ++ ") (" ++ show query ++ ")"
    liftIO $ putStrLn msg
    dbLock <- new
    x <- liftIO $ handler dbLock path query
    respond x

handler l ["new-result"] [ ("name", Just pn)
                            , ("sport", Just sp)
                            , ("gender", Just g )
                            , ("score", Just sc)
                            , ("city", Just c)
                            , ("proof", Just p)
                            , _
                            ] = newResult l pn sp g sc c p
handler _ ["scoreboard"] [("sport", Just s)] =
    case s of
        "Kaikki" -> scoreboard Nothing
        "Henkkari" -> scoreboard (Just Henkkari)
        "Viisiottelu" -> scoreboard (Just Viisiottelu)
        "Seiskaottelu" -> scoreboard (Just Seiskaottelu)
        "Joukkis" -> scoreboard (Just Joukkis)
        "Sviippitreeni" -> scoreboard (Just Sviippitreeni)
        "Pystyhydra" -> scoreboard (Just Pystyhydra)
        _ -> scoreboard Nothing
handler _ ["scoreboard"] _ = scoreboard Nothing
handler _ ["info"] _ = info
handler _ ["index"] _ = scoreboard Nothing
handler _ path query = invalid

newResult l playerName sport gender score city proof = do
    updateDB l $ parseResult playerName sport gender score city proof
    return (responseLBS status200 [ ("Content-Type", "text/html")
                                  , ("charset", "utf-8")
                                  ] resultInputPage)

scoreboard filter = do
    maybeDB <- liftIO getDB
    case maybeDB of
        Nothing ->
            return (responseLBS status200
                    [ ("Content-Type", "text/html")
                    , ("charset", "utf-8")
                    ] $ (scoreboardPage [] filter))
        Just db -> do
            let results = map (read . T.unpack) $ T.lines db
            return (responseLBS status200
                    [ ("Content-Type", "text/html")
                    , ("charset", "utf-8")
                    ] $ (scoreboardPage results filter))

info = return $
    responseLBS status200
        [("Content-Type", "text/html"), ("charset", "utf-8")] infoPage


invalid = return $
    responseLBS status200
        [("Content-Type", "text/html"), ("charset", "utf-8")] resultInputPage

