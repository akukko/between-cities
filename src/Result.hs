{-# LANGUAGE OverloadedStrings #-}


module Result ( parseResult
              , shortCity
              , shortSport
              , Result(..)
              , Sport(..)
              , Series(..)
              , City
              ) where

import Data.Text
import Data.Text.Encoding as TE
import Data.Either
import Debug.Trace



data Result = Result
    { player :: Text
    , sport :: Sport
    , series :: Series
    , score :: Float
    , city :: City
    , proof :: Text
    } deriving (Show, Read)




data Series
    = Miehet
    | Naiset
    deriving (Show, Read)

data Sport
    = Henkkari
    | Viisiottelu
    | Seiskaottelu
    | Joukkis
    | Pystyhydra
    | Sviippitreeni
    deriving (Show, Read, Eq)

data City
    = Tampere
    | Oulu
    | Lappeenranta
    | Helsinki
    | Rauma
    | Muu
    deriving (Show, Read, Eq, Ord)

shortSport Henkkari = "HK"
shortSport Viisiottelu = "5O"
shortSport Seiskaottelu = "7O"
shortSport Joukkis = "HJK"
shortSport Pystyhydra = "PH"
shortSport Sviippitreeni = "SS"

shortCity Tampere = "TRE"
shortCity Oulu = "OUL"
shortCity Lappeenranta = "LPR"
shortCity Helsinki = "HEL"
shortCity Rauma = "RAU"
shortCity Muu = "MUU"

parseResult p spt ser scr c prf = Result plr sport series score city proof
  where

    plr = strip $ fromRight "Invalid player name" $ TE.decodeUtf8' p
    sport = case spt of
                "Henkkari" -> Henkkari
                "5-ottelu" -> Viisiottelu
                "7-ottelu" -> Seiskaottelu
                "Joukkis" -> Joukkis
                "Sviippitreeni" -> Sviippitreeni
                "Pystyhydra" -> Pystyhydra
                _ -> error "Tuntematon laji inputissa"
    series = case ser of
                "Miesten" -> Miehet
                "Naisten" -> Naiset
                _ -> error "Tuntematon sarja inputissa"

    score = read $ unpack $ strip $ fromRight "-200" $ TE.decodeUtf8' scr
    --score = read (show scr) :: Float
    city = case c of
               "Tampere" -> Tampere
               "Oulu" -> Oulu
               "Lappeenranta" -> Lappeenranta
               "Helsinki" -> Helsinki
               "Rauma" -> Rauma
               "Muu" -> Muu

    proof = fromRight "Invalid proof text" $ TE.decodeUtf8' prf
