# between-cities

This is a quick and dirty application for inputting the results of 
Kaupunkienväliset kyykkä competition.

The main driving force behind the app is:
- Doing the bare minimum to achieve required functionality
- Avoiding writing any JavaScript

Merge requests are welcome.
